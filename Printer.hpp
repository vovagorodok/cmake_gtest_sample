#pragma once

#include <iostream>

struct IPrinter
{
    virtual void print(int) = 0;
    virtual ~IPrinter() = default;
};

struct Printer : IPrinter
{
    void print(int var) override
    {
        std::cout << "number " << var << std::endl;
    }
};