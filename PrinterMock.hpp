#pragma once

#include "gmock/gmock.h"
#include "Printer.hpp"

struct PrinterMock: IPrinter
{
    MOCK_METHOD1(print, void(int));
};