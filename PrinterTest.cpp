#include "gtest/gtest.h"
#include "PrinterMock.hpp"

struct PrinterTest : public testing::Test
{
    PrinterMock mock{};
    Printer sut{};
};

TEST_F(PrinterTest, test)
{
    EXPECT_CALL(mock, print(2));
    mock.print(2);
}
